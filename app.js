var restify = require("restify");
var server = restify.createServer();
var socketio = require("socket.io");
var os = require("os");
var io = socketio.listen(server.server);
const redisAdapter = require("socket.io-redis");
if (process.env.NODE_ENV === "pro") {
  require("custom-env").env("pro");
} else {
  // dev
  require("custom-env").env();
}
console.log(process.env.ENVIRONMENT);
io.adapter(
  redisAdapter({ host: process.env.REDIS_HOST, port: process.env.REDIS_PORT })
);

// io.adapter(redisAdapter({ host: "redis", port: 6379 }));

function respond(req, res, next) {
  res.send(os.hostname() + " hello " + req.params.name);
  next();
}

server.get("/hello/:name", respond);
server.head("/hello/:name", respond);

io.on("connection", function(socket) {
  socket.on("msg", function(data) {
    console.log(data);
    socket.emit("new_msg", data);
    socket.broadcast.emit("new_msg", data);
  });
});

io.of("/").adapter.allRooms((err, rooms) => {
  console.log(rooms); // an array containing all rooms (accross every node)
});

server.on(
  "after",
  restify.plugins.metrics({ server: server }, function onMetrics(err, metrics) {
    console.log(
      `${metrics.method} ${metrics.path} ${metrics.statusCode} ${metrics.latency} ms`
    );
  })
);

console.log(process.env.PORT);
if (process.env.PORT) {
  server.listen(process.env.PORT, function() {
    console.log("%s listening at %s", server.name, server.url);
  });
} else {
  server.listen(8080, function() {
    console.log("%s listening at %s", server.name, server.url);
  });
}
