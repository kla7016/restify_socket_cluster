## วิธีติดตั้งโปรเจค Restify Socket Cluster

### 1) ติดตั้ง Redis

เข้าไปยัง Folder redis

```
cd redis
```

ใช้ Docker รัน redis ขึ้นมา

```
docker-compose -f redis.yml up -d
```

### 2) Build Image

Build ที่ directory ที่มี Dockerfile

```
docker build -t restify_socket .
```

### 3) Run Service Socket ตัวที่ 1 ที่ Port 8080

```
docker-compose up -d
```

### 4) รัน Node ที่ 2 และ 3

```
cd other_socket_node
```

รัน Node ที่ 2 ที่ port 8081

```
docker-compose -f node2.yml up -d
```

รัน Node ที่ 3 ที่ port 8082

```
docker-compose -f node3.yml up -d
```

### 5) รัน Nginx

```
docker-compose -f nginx.yml up -d
```

nginx จะรันที่ port 8000
หน้าที่ของ Nginx ในโปรเจคนี้คือ ทำ sticky session
คือถ้า User เข้ามาด้วย ip เดิม จะวิ่งที่ไปที่ Server เครื่องเดิม

## การทดสอบ

```
ลองสร้าง event "new_msg" ที่ Socket 8080, 8081, 8082
```

```
ลอง emit ข้อความไปที่ event "msg"  แล้วไปดูที่ event "new_msg" ที่เราสร้างไว้ ทุก Node จะต้องได้รับข้อความที่ emit
```

#### ถ้าเป็น Production Client สามารถต่อที่ Port 8000 ได้เลย

### Diagram

![alt text](https://sv1.picz.in.th/images/2019/11/13/g2EcFl.jpg)

### Lib ที่ใช้

[http://restify.com/](http://restify.com/)

[https://github.com/socketio/socket.io-redis](https://github.com/socketio/socket.io-redis)

### เครื่องมือในการ Dev

[https://pm2.keymetrics.io/](https://pm2.keymetrics.io/)

[https://ngrok.com/](https://ngrok.com/)
